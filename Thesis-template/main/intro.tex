

\chapter{Introduction}
\label{chap:Intro}
% Introduction

Decision making under uncertainty and belief space planning are  fundamental problems in robotics and artificial intelligence, with applications including autonomous driving, surveillance, sensor deployment, object manipulation and active SLAM. The goal is to autonomously determine best actions according to a specified objective function, given the current belief about random variables of interest that could represent, for example, robot poses, tracked target or mapped environment, while accounting for different sources of uncertainty. 

Since the true state of interest is typically unknown and only partially observable through acquired measurements, it can be only represented through a probability distribution conditioned on available data. Belief space planning (BSP) and decision making approaches reason how this distribution (the \emph{belief}) evolves as a result of candidate actions and future expected observations. Such a problem is an instantiation of partially observable Markov decision process (POMDP), while calculating an optimal solution of POMDP was proven to be computationally intractable \cite{Kaelbling98ai} for all but the smallest problems due to curse of history and curse of dimensionality. Recent research has therefore focused on the development of sub-optimal approaches that trade-off optimality and runtime complexity. 

Decision making under uncertainty, also sometimes referred to as active inference, and BSP can be formulated as selecting optimal action from a set of candidates, based on some cost function. In information-based decision making the cost function typically contains terms that evaluate the expected posterior uncertainty upon action execution, with commonly used costs including (conditional) entropy and mutual information. Thus, for Gaussian distributions the corresponding calculations typically involve calculating a determinant of a posteriori covariance (information) matrices, and moreover, these calculations are to be performed for \emph{each} candidate action. 

Decision making and BSP become an even more challenging problems when considering \emph{high} dimensional state spaces. Such a setup is common in robotics, for example in the context of belief space planning in uncertain environments, active SLAM, sensor deployment, graph reduction and graph sparsification. In particular, calculating a determinant of information (covariance) matrix for an $n$-dimensional state is in general $O(n^3)$, and is smaller for sparse matrices as in SLAM problems \cite{Bai96jcam}. 

Moreover, state of the art approaches typically perform such time-consuming calculations from scratch for \emph{each} candidate action. In contrast, in this work we provide a novel way to perform information-theoretic BSP, which is fast, simple and general; yet, it does not require calculation of a posterior belief and does not need determinant computation of large matrices. Additionally, we have succeeded to reuse calculations between different candidate actions, eventually providing decision making solver which is significantly faster compared to standard approaches.


\vspace{-5pt}


\section{Related Work}
\label{sec:related-work}

As mentioned above, the optimal solution to POMDP is computationally intractable and many approximation approaches exist to solve it in sub-optimal way. These approaches can be classified into those that discretize the action, state and measurement spaces, and those that operate over continuous spaces. 

Approaches from the former class include point-based value iteration methods \cite{Pineau06jair}, simulation based \cite{Stachniss05rss} and sampling based approaches \cite{Prentice09ijrr, AghaMohammadi14ijrr}. On the other hand, approaches that avoid discretization are often termed direct trajectory optimization methods (e.g.~\cite{Indelman15ijrr,VanDenBerg12ijrr,Patil14wafr, Walls15iros, Platt10rss}); these approaches typically calculate from a given nominal solution a locally-optimal one. 

 
To solve the BSP problem, standard methods usually perform expensive calculations for each candidate action from scratch.  For example, in the context of active SLAM, state of the art BSP approaches first calculate the posterior belief within the planning horizon, and then use that belief to evaluate the objective function, which typically includes an information-theoretic term \cite{Huang05icra, Kim14ijrr, Valencia13tro, Indelman15ijrr}. These approaches then determine the best action by performing the mentioned calculations for each action from a given set of candidate actions, or by local search using dynamic programming or gradient descent (for continuous setting).

Sensor deployment is another example of decision making in high dimensional state spaces. The basic formulation of the problem is to determine locations to deploy the sensors such that some metric can be measured most accurately through the entire area (e.g.~temperature). The problem can also be viewed as selecting optimal action from the set of candidate actions (available locations) and the objective function usually contains a term of uncertainty, like the entropy of a posterior system \cite{Krause08jmlr}. Also here, state of the art approaches evaluate a determinant over large posterior covariance (information) matrices for each candidate action, and do so from scratch \cite{Zimmerman06env, Zhu06jabes}.

A similar situation also arises in measurement selection \cite{Davison05iccv, Carlone14iros} and graph pruning \cite{Vial11iros, Mazuran14rss, CarlevarisBianco14tro, Huang13ecmr} in the context of long-term autonomy in SLAM. In the former case, the main idea is to determine the most informative measurements (e.g.~image features) given measurements provided by  robot sensors, thereby discarding uninformative and redundant information. Such a process typically involves reasoning about mutual information, see e.g.~\cite{Davison05iccv, Chli09ras}, for each candidate selection. Similarly, graph pruning and sparsification can be considered as instances of decision making in high dimensional state spaces \cite{CarlevarisBianco14tro, Huang13ecmr}, with decision corresponding to determining what nodes to marginalize out \cite{Ila10tro, Kretzchmar12ijrr}, and avoiding the resulting fill-in in information matrix by resorting to sparse approximations of the latter \cite{Vial11iros, Mazuran14rss, CarlevarisBianco14tro, Huang13ecmr}. Also here, existing approaches typically involve calculation of determinant of large matrices for each candidate action.

Although many particular domains can be specified as decision making and BSP problems, they all can be classified into two main categories, one where state vector is fixed during belief propagation and another where the state vector is augmented with new variables. Sensor deployment is an example of the first case, while active SLAM, where future robot poses are introduced into the state, is an example for the second case.
%The example for first one is sensor deployment, and for second - active SLAM where future robot poses are introduced into the state. 
Conceptually the first category is a particular case of the second, but as we will see both will require different solutions. Therefore, in order to differentiate between these two categories, in this research we will consider the first category (fixed-state) as \emph{BSP} problem, and the second category (augmented-state) as \emph{Augmented BSP} problem.

Moreover, we show the proposed concept is applicable also to active \focused inference. Unlike the \unfocused case discussed thus far, active \focused inference approaches aim to reduce the uncertainty over only a predefined set of the variables. The two problems can have significantly different optimal actions, with an optimal solution for the \unfocused case potentially performing badly for the \focused setup, and vice versa (see e.g.~\cite{Levine13nips}). While the set of \focused variables can be small, exact state of the art approaches calculate the marginal posterior covariance (information) matrix, for each action, which involves a computationally expensive Schur complement operation. For example, Mu et al.~\cite{Mu15rss} calculate posterior covariance matrix per each measurement and then use the selection matrix in order to get marginal of \focused set. Levine et al.~\cite{Levine13nips} develop an approach that determines mutual information between \focused and \unfocused variables through message passing algorithms on Gaussian graphs but their approach is limited to only graphs with unique paths between the relevant variables.%, making it hard to apply in real applications.


Finally, there is also a relation to the recently introduced concept of decision making in a conservative sparse information space \cite{Indelman15acc, Indelman16ral}. In particular, considering unary observation models (involving only one variable) and greedy decision making, it was shown that appropriately dropping all correlation terms and remaining only with a diagonal covariance (information) matrix does not sacrifice performance while significantly reducing computational complexity. While the approach presented herein confirms this concept for the case of unary observation models, our approach addresses a general non-myopic decision making problem, with arbitrary observation and motion models.


\vspace{-5pt}

\section{Contributions}

In this thesis we develop a computationally efficient and exact approach for decision making and BSP in high-dimensional state spaces that addresses the aforementioned challenges. The \emph{key idea} is to use the (augmented) general matrix determinant lemma to calculate  action impact with complexity \emph{independent} of state dimensionality $n$, while \emph{re-using} calculations between evaluating impact for different candidate actions. Our approach supports general observation and motion models, and non-myopic planning, and is thus applicable to a wide range of applications such as those mentioned above, where fast decision making and BSP in high-dimensional state spaces is required.

For \focused BSP scenarios we present a new way to calculate posterior entropy of \focused variables, which is very computationally efficient, yet exact and does not require expensive calculation of a Schur complement and a posterior covariance matrix. In combination with our \emph{re-use} algorithm, it provides \focused decision making algorithm which is significantly faster compared to state of the art approaches.

Calculating the posterior information matrix in Augmented BSP problems involves augmenting an appropriate prior  information matrix with zero rows and columns, i.e.~zero padding, and then adding new information due to candidate action (see Figure \ref{fig:AugmentInfoMAtBSFig}). While the general matrix determinant lemma is an essential part of our approach, unfortunately it is not applicable to the mentioned augmented prior information matrix since the latter is singular (even though the posterior information matrix is full rank). In this thesis, we develop a new variant of the matrix determinant lemma, called the \emph{augmented matrix determinant lemma} (AMDL), that addresses general augmentation of future state vector. Based on AMDL, we then develop a augmented belief space planning approach, considering both \unfocused and \focused cases.

To summarize, our main contributions in this research are as follows: 
(a) we formulate (augmented) belief space planning in terms of factor graphs which allow to see the problem in more intuitive and simple way; 
(b) we develop an augmented version of matrix determinant lemma (AMDL), where the subject matrix first is augmented by zero rows/columns and only then new information is introduced 
(c) we develop an approach for a nonmyopic \focused and \unfocused (augmented) belief space planning in high-dimensional state spaces that uses the (augmented) matrix determinant lemma to avoid calculating determinants of large matrices, with per-candidate complexity independent of state dimension; 
(d) we show how calculations can be \emph{re-used} when evaluating impacts of different candidate actions; 
We integrate the calculations \emph{re-use} concept and AMDL into a general and highly efficient BSP solver, that does not involve explicit calculation of posterior belief evolution for different candidate actions, naming this approach \emph{rAMDL}; 
(e) we introduce an even more efficient \rudl variant specifically addressing a sensor deployment problem.

\section{Organization}
This thesis is organized as follows.
\begin{enumerate}
\item{Chapter \ref{chap:ProbDef} introduces the concepts of BSP, and gives a formal statement of the problem.}
\item{Chapter \ref{chap:Approach}  describes our approach \rudl for general formulation.}
\item{Chapter \ref{chap:SpecCases} tailors approach for specific domains, providing even more efficient solutions to number of them.}
\item{In Chapter \ref{chap:StandardTech} standard approaches are discussed as the main state-of-the-art alternatives to \rudl.}

\item{Chapter \ref{chap:Res} presents experimental results, evaluating the proposed approach and comparing it against mentioned state-of-the-art.}
\item{Conclusions are drawn in Chapter \ref{chap:Conclus}.}
\item{For purpose of simplicity, the proof of several lemmas is moved into Appendix \ref{chap:AppndSec}.}
\end{enumerate}
