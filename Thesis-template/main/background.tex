

\chapter{Notations and Problem Formulation}
\label{chap:notations}

Planning and decision making under uncertainty are fundamental problems in the area of autonomous navigation. In the presence uncertainty arising, for example, due to stochastic robot motion and imperfect sensing, the true state over variables of interest, such as robot poses, is unknown and can be only represented by a probability distribution function (pdf), also known as the \emph{belief}. The corresponding planning problem is known as belief space planning (BSP), which is an instantiation of a partially observable Markov decision problem (POMDP) \cite{Kaelbling98ai}. Such a framework is applicable to numerous problem domains and applications such as active simultaneous localization and mapping (SLAM), active sensing, informative planning, and additional variants of autonomous navigation.

\section{Notations and Problem Formulation}
\label{sec:notations}

Consider a robot, uncertain about its pose, operating in a partially known or pre-mapped environment. The robot takes observations of different scenes or objects in the environment and uses these observations to infer random variables of interest which are application-dependent. Thus, in localisation, these observations can be used to better estimate the robot pose, while in search and rescue missions one is looking for survivors in a certain region. 

A schematic equivalent to this is shown in  Figure~\ref{fig:schema_spaces}. As can be seen, it involves three spaces: \emph{pose-space}, \emph{scene-space} and \emph{observation-space}. \emph{Pose-space} involves all possible perspectives a robot can take with respect to a given world model and in the context of task at hand. 

We shall denote a particular pose at any time step $k$ as $x_k$, and the sequence of these poses from $0$ up to $k$ as $X_k\doteq \{x_0,\ldots,x_k\}$. By uncertainty in robot's pose, we mean that the current pose of robot at any step $k$, is known only through a posterior probability distribution function (pdf) $\prob{X_k|u_{0:k-1},Z_{0:k}}$ given all controls $u_{0:k-1}\doteq \{u_0,\ldots,u_{k-1}\}$ and observations $Z_{0:k}\doteq \{Z_0,\ldots,Z_k\}$ up to time $k$. For notational convenience, we define  histories $\mathcal{H}_k$ and $\mathcal{H}_{k+1}^-$ as 
%
\begin{eqnarray}
\mathcal{H}_k\doteq \{u_{0:k-1},Z_{0:k}\} \ \ , \ \ \mathcal{H}_{k+1}^- \doteq \mathcal{H}_k \cup \{u_k\}.
\end{eqnarray}
%
and we rewrite the posterior at time $k$ as $b[X_k]\doteq\prob{X_k|\mathcal{H}_k}$.



In contrast, \emph{scene-space} involves a discrete set of objects or scenes, denoted by the set \events, in the given world model, and which can be detected through the sensors of the robot. We shall use symbols \event{i} and \event{j} to denote such typical scenes. Note that even if the objects are identical, they are distinct in scene space. This is important when we shall consider the cases where the objects look similar from some perspectives. Finally, \emph{observation-space} is the set of all possible observations that the robot is capable of obtaining when considering its mission and sensory capabilities. 

We shall consider such an observation as the model:
%
\begin{eqnarray}
z_{k} = h(x_k,A_i)+v_k \ \ , \ \ v_k \sim \mathcal{N}(0,\Sigma_v), \label{eq:observationModel}
\end{eqnarray}
%
and represent it probabilistically as $\prob{z_k|x_k,A_i}$. Here we have assumed the same Gaussian noise for all observations irrespective of the scenes being observed. This is a reasonable assumption, since such noise would be a typical property of the robotic sensors employed. Also, $h(x_k, \event{i})$ is a noise-free observation which we would refer as \emph{nominal} observation $\hat z$. 



For example, in case of a camera the function $h$ could be defined as a pinhole projection operator, thereby projecting the object $A_i$ onto the image plane, while in case of a range sensor this function calculates the range between (a particular point on) the object and the robot actual location.

Note that the exposition thus far is equivalently valid also in case where the environment model is given but uncertain, and when this model is unknown a priori and instead is constructed on-line within SLAM framework. %From an estimation point of view, these cases are different - only in the latter case, the environment model is part of the inference process and thus will be refined upon acquiring new observations. 

% notations + motion and observation models
%Let $x_k$ and $X_{k}$ denote, respectively, robot pose at time $k$ and all poses up to that time, i.e.~$X_k\doteq \{x_0,\ldots,x_k\}$. The posterior probability distribution function (pdf) over $X_k$ given all controls $u_{0:k-1}\doteq \{u_0,\ldots,u_{k-1}\}$ and observations $z_{0:k}\doteq \{z_0,\ldots,z_k\}$ up to time $k$ is given by $\prob{X_k|u_{0:k-1},z_{0:k}}$. For notational convenience, we rewrite the latter as $\prob{X_k|\mathcal{H}_k}$, where the history $\mathcal{H}_k$ is defined as $\mathcal{H}_k\doteq \{u_{0:k-1},z_{0:k}\}$.

We also consider a standard motion model $x_{i+1} = f(x_i,u_i)+w_i$ with Gaussian noise $w_i \sim \mathcal{N}(0,\Sigma_w)$, 
%
%%\begin{eqnarray}
%%x_{i+1} = f(x_i,u_i)+w_i \ \ , \ \ w_i \sim \mathcal{N}(0,\Sigma_w)
%%\end{eqnarray}
%
where $\Sigma_w$ is the process noise covariance, and denote this model probabilistically by $\prob{x_{i+1}|x_i,u_i}$.

Given a prior  $\prob{x_0}$ and motion and observation models, the joint posterior pdf at the current time $k$ can be written as
%
\begin{eqnarray}
\prob{X_k|\mathcal{H}} = \prob{x_0}\prod_{i=1}^k \prob{x_i|x_{i-1},u_{i-1}} \prob{Z_i|x_i,A_i}.
\label{eq:posterior_k}
\end{eqnarray} 
%
This  pdf is thus a  Gaussian $\prob{X_k|\mathcal{H}_k}=\mathcal{N}(\hat{X}_k,\Sigma_k)$ with mean $\hat{X}_k$ and covariance $\Sigma_k$ that can be efficiently calculated via maximum a posteriori (MAP) inference, see e.g.~\cite{Kaess12ijrr}.

It is important to note that the underlying assumption in  factorisation (\ref{eq:posterior_k}) is that it is known which object is being observed at each time $i$, i.e.~data association is given and error-free. We will come back to this key point in the sequel.
 
% Objective function - single step
Given the posterior (\ref{eq:posterior_k}) at the current time $k$, one can reason about the robot's best future actions that would minimise (or maximise) a certain objective function. Such a function, for a single look ahead step, is given by
%
\begin{equation}
J(u_{k}) =  \expt{z_{k+1}}{c(\prob{X_{k+1}| \mathcal{H}_{k+1}^-,z_{k+1}})},
\label{eq:ObjFuncGeneralOneStep}
\end{equation}
%
where the expectation is taken about the random variable $z_{k+1}$ with respect to the propagated belief $\prob{X_{k+1}| \his_{k+1}^-}$ to consider all possible realisations of a future observation $z_{k+1}$.

For notational convenience we will often represent the posterior $\prob{X_{k+1}|\mathcal{H}_{k+1}^-,z_{k+1}}$ as the \emph{belief} $b[X_{k+1}]$, i.e.:
% belief
\begin{equation}
b[X_{k+1}]\doteq \prob{X_{k+1}| \mathcal{H}_{k+1}^-,z_{k+1}}.
\label{eq:Belief}
\end{equation}
%
Note that, according to Eq.~(\ref{eq:ObjFuncGeneralOneStep}),  we need to calculate the posterior belief (\ref{eq:Belief}) for \emph{each} possible value of $z_{k+1}$. 

Similarly, we define the propagated joint belief as
\begin{eqnarray}
b[X_{k+1}^-]\doteq \prob{X_{k+1}|\mathcal{H}_{k+1}^-} = \prob{X_{k}|\mathcal{H}_k}  \prob{x_{k+1}|x_{k},u_{k}},
\label{eq:PredictedBelief}
\end{eqnarray}
%
from which the marginal belief over the future pose $x_{k+1}$ can be calculated as $b[x_{k+1}^-] \doteq \int_{\lnot x_{k+1}} b[X_{k+1}^-]$.


As earlier, if data association is assumed given and perfect as commonly done in BSP, then one can consider for each specific value of $z_{k+1}$ the corresponding observed scene $A_i$, and express the posterior (\ref{eq:Belief}) as
%
\begin{equation}
b[X_{k+1}] \!=\! \eta \prob{X_{k}|\mathcal{H}_k}  \prob{x_{k+1}|x_{k},u_{k}} 
 \prob{z_{k+1}|x_{k+1},A_i},
\label{eq:Posterior_givenAi} 
\end{equation}
%
which can be represented as $b[X_{k+1}]=\mathcal{N}(\hat{X}_{k+1}, \Sigma_{k+1})$ with appropriate mean $\hat{X}_{k+1}$ and covariance $\Sigma_{k+1}$.

The objective function (\ref{eq:ObjFuncGeneralOneStep}) can be now evaluated, given a candidate action $u_k$, by calculating the cost $c(.)$ for each $z_{k+1}$. Finally, the optimal action $u^{\star}_k$ is defined as $u^{\star}_k = \argmin_{u_{k}} J(u_{k})$.

Assuming data association to be given and perfect simplifies greatly the above formulation. Yet, in practice, determining data association reliably is often a non trivial task by itself, especially when operating in perceptually aliased environments. An incorrect data association (wrong scene $A_i$ in Eq.~(\ref{eq:Posterior_givenAi})) can lead to catastrophic results, see, e.g.~\cite{Indelman14icra, Indelman14rss_ws, Indelman16csm}. In this work we relax this restricting assumption and rigorously incorporate data association aspects within belief space planning.




