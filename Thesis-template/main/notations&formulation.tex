
\chapter{Notations and Problem Formulation}
\label{chap:ProbDef}

%%
%\begin{figure}[!t]
%  \centering
%  \subfloat[\label{fig:SLAMMiniExampleFigPart}]{\includegraphics[width=0.45\textwidth]{Figures/SLAMExampleGraphModel/SLAM.png}}
%  \hspace{10pt}
%  \subfloat[\label{fig:SLAMFactorGraphFig}]{\includegraphics[width=0.45\textwidth]{Figures/FactorGraphExample/drawing.png}}
%\protect
%  \caption{(a) Graphical model describing motion and observation models in SLAM scenario (Eq.~(\ref{eq:MotionModel}) and Eq.~(\ref{eq:ObservModel})). For simplicity, the figure shows observations of only a single landmark $l_j$, indicating it is observed at time instances $t_{i-1}$, $t_i$ and $t_{i+1}$.
%(b) Factor graph that represents state variables and information from (a). $f_{1}$ and $f_{2}$ are factors of motion model, and $f_{3}$-$f_{5}$ are factors of observation model.}
%\label{fig:SLAMMiniExampleFig}
%\end{figure}
%%




In this thesis we are developing computationally efficient approaches for belief space planning. As evaluating action impact involves inference over an appropriate posterior, we first formulate the corresponding inference problem.


%In this thesis we are discussing BSP within inference problems where system state is unknown and is estimated through acquired measurements. Thus problem requires first to solve inference itself.


Consider a high-dimensional problem-specific state vector $X_k\in\mathbb{R}^{n}$ at time $t_k$. In different applications the state $X_k$ can represent robot configuration and poses (optionally for whole history), environment-related variables or any other variables to be estimated. Additionally, consider factors $F_i = \{f_{i}^{1}(X_{i}^{1}),\ldots,f_{i}^{n_i}(X_{i}^{n_i}) \}$ that were added at time $0 \leq t_i \leq t_k$, where each factor $f_{i}^{j}(X_{i}^{j})$  represents a specific measurement model, motion model or prior, and as such involves appropriate state variables $X_{i}^{j} \subseteq X_i$.

%Each factor set $F_{i}$ contains factors $\{f_{i}^{1}(X_{i}^{1}),\ldots,f_{i}^{n_i}(X_{i}^{n_i}) \}$ that were added to graph at time $0 \leq t_i \leq t_k$, where each factor $f_{i}^{j}(X_{i}^{j})$ is representing specific measurement model, motion model or prior and as such involves appropriate state variables $X_{i}^{j} \subseteq X_i$.

% and factor sets $F_{0:k}$, all factors acquired up to $t_k$
 
 
The joint pdf can be then written as
\begin{equation}
\prob{X_{k}|\mathbf{H_k}} \propto
 \prod_{i=0}^{k}
 \prod_{j=1}^{n_i}
 f_{i}^{j}(X_{i}^{j}), 
 \label{eq:FactorPDF}
\end{equation}
%
where $\mathbf{H_k}$ is history that contains all the information gathered till time $t_k$ (measurements, controls, etc.).


As common in many inference problems, we will assume that all factors have a Gaussian form:
%
\begin{equation}
f_{i}^{j}(X_{i}^{j}) \propto 
\exp (- \half \Vert h_{i}^{j}(X_{i}^{j}) - r_{i}^{j} \Vert_{\Sigma_{i}^{j}}^{2}),
\label{eq:FactorModel}
\end{equation}
%
with appropriate model
%
\begin{equation}
r_{i}^{j} = h_{i}^{j}(X_{i}^{j}) + \upsilon_{i}^{j}, \quad
\upsilon_{i}^{j} \sim \mathcal{N}(0, \Sigma_{i}^{j})
\label{eq:FactorModel2}
\end{equation}
%
where $h_i^j$ is a known nonlinear function, $\upsilon_{i}^{j}$ is zero-mean Gaussian noise and $r_i^j$ is the expected value of $h_i^j$ ($r_i^j = \mathbb{E}[h_{i}^{j}(X_{i}^{j})]$). Such a factor representation is a general way to express information about the state. In particular, it can represent a measurement model, in which case, $h_i^j$ is the observation model, and  $r_i^j$ and $\upsilon_i^j$ are, respectively, the actual measurement $z$ and measurement noise. Similarly, it can also represent a motion model (see Section \ref{sec:SLAMPSection}). A maximum a posteriori (MAP) inference can be efficiently calculated (see e.g.~\cite{Kaess12ijrr}) such that
\begin{equation}
\prob{X_{k}|\mathbf{H_k}} = \mathcal{N}(X_k^*, \Sigma_k),
\end{equation}
%
where $X_k^*$ and $\Sigma_k$ are the mean vector and covariance matrix, respectively. 


We shall refer to the posterior $\prob{X_{k}|\mathbf{H_k}}$ as the belief and write
\begin{equation}
b[X_k]\doteq \prob{X_{k}|\mathbf{H_k}}.
\end{equation}


In the context of BSP, we typically reason about the evolution of future beliefs $b[X_{k+l}]$ at different look-ahead steps $l$ as a result of different candidate actions. Particular candidate action can provide unique information (future observations and controls) and can be more and less beneficial for specific tasks such as reducing future uncertainty. For example, in SLAM application choosing trajectory that is close to the mapped landmarks will reduce uncertainty because of loop-closures. Furthermore, conceptually each candidate action can introduce different additional state variables into the future state vector,  like in case of smoothing SLAM formulation where state is augmented by (various) number of future robot poses. 


Therefore, in order to reason about the belief $b[X_{k+l}]$, first it needs to be carefully modeled. More specifically, let us focus on a non-myopic candidate action $a \doteq \{ \bar{a}_1, \ldots, \bar{a}_L\}$ which is a sequence of myopic actions with planning horizon $L$. Each action $\bar{a}_l$ can be represented by new factors $F_{k+l} = \{f_{k+l}^{1}(X_{k+l}^{1}),\ldots,f_{k+l}^{n_{k+l}}(X_{k+l}^{n_{k+l}}) \}$ and, possibly, new state variables $X_{new}^{k+l}$ ($1 \leq l \leq L$) that are acquired/added while applying $\bar{a}_l$. Similar to Eq.~(\ref{eq:FactorPDF}), the future \emph{belief} $b[X_{k+L}]$ can be explicitly written as 
\begin{equation}
b[X_{k+L}] \propto
b[X_k]
 \prod_{l=k+1}^{k+L}
 \prod_{j=1}^{n_l}
 f_{l}^{j}(X_{l}^{j}), 
	\label{eq:FactorBeliefPropogation}
\end{equation}
%
where $X_{k+L} \doteq \{ X_{k} \cup X_{new}^{k+1} \cup \ldots \cup X_{new}^{k+L} \}$ contains old and new state variables. Similar expressions can be also written for  any other look ahead step $l$. Observe in the above \emph{belief} that the future factors depend on future observations, whose actual values are unknown.


%, as will be seen below, evaluating the information-theoretic term only involves the Jacobians and not the actual values of future measurements. \VI{[suggest to move below where this is issue is discussed, or remove]}
%\DK{[possible, but for me it looks good. it prepares the reader to the idea that measurements are not needed. but it s not critical.]}

%It is important to note that no new variables are added in not augmented setting of BSP problem, according to our definition from Section \ref{sec:Intro}. 

It is important to note that, according to our definition from Chapter \ref{chap:Intro}, new variables are added only in the augmented setting of the BSP problem, e.g.~in the active SLAM context. On the other hand, in a non-augmented BSP setting, the states $X_{k+L}$ and $X_k$ are identical, while the beliefs $b[X_{k+L}]$ and $b[X_k]$ are still conditioned on different data. For example, in sensor deployment and measurement selection problems the candidate actions are all possible subsets of sensor locations and of acquired observations, respectively. Here, when applying a candidate action, new information about $X_k$ is brought in, but the state vector itself is unaltered. 


In contrast, in Augmented BSP problem new variables are always introduced. In particular, in both smoothing and filtering formulation of SLAM, candidate actions (trajectories) will introduce both new information (future measurements), and also new variables (future robot poses). While in \emph{filtering} formulation old pose variables are marginalized out, the \emph{smoothing} formulation instead keeps past and current robot poses and newly mapped landmarks in the state vector which is beneficial for better estimation accuracy and sparsity. As such, smoothing formulation is an excellent example for Augmented BSP problem, where as filtering formulation can be considered as \focused BSP scenario which described below.


As such the non-augmented BSP setting can be seen as a special case of Augmented BSP. In order to use similar notations for both problems, however, in this thesis we will consider $X_{new}^{k+l}$ to be an empty set for the former case and non-empty for Augmented BSP.


It is not difficult to show (see e.g.~\cite{Indelman15ijrr}) that in case of non-augmented BSP the posterior information matrix of the \emph{belief} $b[X_{k+L}]$ is given by:
%
\begin{equation}
\Lambda_{k+L} = 
\Lambda_{k} + 
\sum_{l=k+1}^{k+L}
\sum_{j=1}^{n_l}
(H_l^{j})^T  \cdot (\Sigma_{l}^{j})^{-1} \cdot H_l^{j}
\label{eq:FactorPosteriorInfoMatrixBSSigmDM}
\end{equation}
%
where $\Lambda_{k}$ is prior information matrix and $H_l^j \doteq \bigtriangledown_x h_{l}^{j}$ are the Jacobian matrices of $h_{l}^{j}$ functions (see Eq.~(\ref{eq:FactorModel})) for all the new factor terms in Eq.~(\ref{eq:FactorBeliefPropogation}).

%
%\begin{figure}[!t]
%  \centering
%  \subfloat{\includegraphics[width=0.6\textwidth]{../Conferences/2017 ICRA/Figures/AugmentBSP/AugmentIllustr.png}}
%\protect
%  \caption{Illustrution of $\Lambda_{k+L}$'s construction for a given candidate action in Augmented BSP case. First, $\Lambda_{k+L}^{Aug}$ is created by adding $n'$ zero rows and columns. Then, the new information of belief is added through $\Lambda_{k+L} = \Lambda_{k+L}^{Aug} + A^T  A$.}
%\label{fig:AugmentInfoMAtBSFig}
%\end{figure}
%


As was already mentioned, in case of Augmented BSP the joint state $X_{k+L}$ includes also new variables (with respect to the current state $X_k$). Considering $X_k \in \mathbb{R}^{n}$, first, new $n'$ variables are introduced into future state vector $X_{k+L} \in \mathbb{R}^{N}$ with $N \doteq n+n'$, and then  new factors involving appropriate variables from $X_{k+L}$ are added to form a posterior \emph{belief} $b[X_{k+L}]$, as shown in Eq.~(\ref{eq:FactorBeliefPropogation}).

Consequently, in Augmented BSP scenario the posterior information matrix of \emph{belief} $b[X_{k+L}]$, i.e.~$\Lambda_{k+L}$, can be constructed by first augmenting the current information matrix $\Lambda_{k}$ with $n'$ zero rows and columns to get $\Lambda_{k+L}^{Aug} \in \mathbb{R}^{N \times N}$, and thereafter adding to it new information, as illustrated in Figure \ref{fig:AugmentInfoMAtBSFig} (see e.g.~\cite{Indelman15ijrr}):
%
\begin{equation}
\Lambda_{k+L} = 
\Lambda_{k+L}^{Aug} + 
\sum_{l=k+1}^{k+L}
\sum_{j=1}^{n_l}
(H_l^{j})^T  \cdot (\Sigma_{l}^{j})^{-1} \cdot H_l^{j}
\label{eq:FactorPosteriorInfoMatrixBSSigmBSP}
\end{equation}
%
where $H_l \doteq \bigtriangledown_x h_{l}^{j}$ are augmented Jacobian matrices of all new factors in Eq.~(\ref{eq:FactorBeliefPropogation}), linearized about the current estimate of $X_k$ and about initial values of newly introduced variables.

After stacking all new Jacobians in Eqs.~(\ref{eq:FactorPosteriorInfoMatrixBSSigmDM}) and (\ref{eq:FactorPosteriorInfoMatrixBSSigmBSP}) into a single matrix $\widetilde{A}$, and combining all noise matrices into block-diagonal $\Psi$, we get respectively
%
\begin{equation}
\Lambda_{k+L} = 
\Lambda_{k} + \widetilde{A}^T \cdot \Psi^{-1} \cdot \widetilde{A} =
\Lambda_{k} + A^T \cdot A
\label{eq:FactorPosteriorInfoMatrixBSDM}
\end{equation}
%
%
\begin{equation}
\Lambda_{k+L} = 
\Lambda_{k+L}^{Aug} + \widetilde{A}^T \cdot \Psi^{-1} \cdot \widetilde{A} =
\Lambda_{k+L}^{Aug} + A^T \cdot A
\label{eq:FactorPosteriorInfoMatrixBSBSP}
\end{equation}
%
where
%
\begin{equation}
A \doteq \Psi^{-\half} \cdot \widetilde{A}
\label{eq:AMatrixComb}
\end{equation}
%
is an $m \times N$ matrix that represents both Jacobians and noise covariances of all new factor terms in Eq.~(\ref{eq:FactorBeliefPropogation}). The above equations can be considered as a single iteration of Gauss-Newton optimization and, similar to prior work \cite{VanDenBerg12ijrr, Kim14ijrr, Indelman15ijrr}, we take maximum-likelihood assumption by assuming they sufficiently capture the impact of candidate action. Under this assumption, the posterior information matrix $\Lambda_{k+L}$ is independent of (unknown) future observations \cite{Indelman15ijrr}. One can further incorporate reasoning if a future measurement will indeed be acquired \cite{Indelman15ijrr, Chaves15iros, Walls15iros}; however, this is outside the scope of this research.

Each block row of matrix $A$ represents a single factor from new terms in Eq.~(\ref{eq:FactorBeliefPropogation}) and has a \emph{sparse} structure. Only a limited number of its sub-blocks is non-zero, i.e.~sub-blocks that correspond to the \emph{involved} variables $X_{l}^{j}$ in the relevant factor $f_{l}^{j}(X_{l}^{j})$.

For notational convenience, we define the set of non-myopic candidate actions by $\mathcal{A}=\{a_1,a_2,...\}$ with appropriate Jacobian matrices $\Phi_A=\{A_1,A_2,...\}$. While the planning horizon is not explicitly shown, each $a \in \mathcal{A}$ can represent a future \emph{belief} $b[X_{k+L}]$ for different number of look ahead steps $L$.

A general objective function in decision making/BSP can be written as \cite{Indelman15ijrr}:
%
\begin{equation}
J(a) \doteq \mathop{\mathbb{E}}_{Z_{k+1:k+L}} \bigg\{ 
\sum_{l=0}^{L-1} c_l(b[X_{k+l}], u_{k+l}) + c_L(b[X_{k+L}]) 
 \bigg\},
\label{eq:GeneralBSPObjFunc}
\end{equation}
%
with $L$ immediate cost functions $c_l$, for each look-ahead step, and one cost function for terminal future \emph{belief} $c_L$. Each such cost function can include a number of different terms related to aspects such as information measure of future \emph{belief}, distance to goal and energy spent on control. Arguably, evaluating the information terms involves the heaviest calculations of $J$. 

Thus, in this research we will focus only on the information-theoretic term of terminal \emph{belief} $b[X_{k+L}]$, and consider differential entropy $\mathcal{H}$ (further referred to just as entropy) and information gain (IG) as the cost functions. Both can measure amount of information of future \emph{belief} $b[X_{k+L}]$, and will lead to the same optimal action. Yet, calculation of one is sometimes more efficient than the other, as will be shown in Chapter \ref{chap:Approach}.
Therefore, we consider two objective functions:
%
\begin{equation}
J_{\mathcal{H}}(a) \doteq \mathcal{H}\left(b[X_{k+L}]\right)
\label{eq:ObjFuncInformation}
\end{equation}
%
%
\begin{equation}
J_{IG}(a) \doteq \mathcal{H}(b[X_{k}]) - \mathcal{H}(b[X_{k+L}]),
\label{eq:ObjFuncInformationIGGeneral}
\end{equation}
%
where the information matrix $\Lambda_{k+L}$, that corresponds to the \emph{belief} $b[X_{k+L}]$, is a function of candidate $a$'s Jacobian matrix $A$, see Eq.~(\ref{eq:FactorPosteriorInfoMatrixBSDM}) and Eq.~(\ref{eq:FactorPosteriorInfoMatrixBSBSP}). The optimal candidate $a^*$, which produces the most certain future \emph{belief}, is then given by $a^* = \argmin_{a \in \mathcal{A}} J_{\mathcal{H}}(a)$, or by $a^* = \argmax_{a \in \mathcal{A}} J_{IG}(a)$ with both being mathematically identical.

In particular, for Gaussian distributions, entropy is a function of the determinant of a posterior information (covariance) matrix, i.e.~$\mathcal{H}\left(b[X_{k+L}]\right)\equiv \mathcal{H}\left(\Lambda_{k+L}\right)$ and the objective functions can be expressed as
%
\begin{equation}
J_{\mathcal{H}}(a) = \frac{n \cdot \gamma}{2} - \frac{1}{2} \ln \begin{vmatrix} \Lambda_{k+L} \end{vmatrix},
\quad
J_{IG}(a) =
\frac{1}{2} \ln \frac{ \begin{vmatrix} \Lambda_{k+L} \end{vmatrix} }
{\begin{vmatrix} \Lambda_k \end{vmatrix}}
\label{eq:EntropyPriorDM}
\end{equation}
%
for BSP, and
%
\begin{equation}
J_{\mathcal{H}}(a) = \frac{N \cdot \gamma}{2} - \frac{1}{2} \ln \begin{vmatrix} \Lambda_{k+L} \end{vmatrix},
\quad
J_{IG}(a) =
\frac{n' \cdot \gamma}{2} +
\frac{1}{2} \ln \frac{ \begin{vmatrix} \Lambda_{k+L} \end{vmatrix} }
{\begin{vmatrix} \Lambda_k \end{vmatrix}}
\label{eq:EntropyPriorBSP}
\end{equation}
%
for Augmented BSP, where $\gamma \doteq 1 + \ln (2 \pi)$, and $\Lambda_{k+L}$ can be calculated according to Eq.~(\ref{eq:FactorPosteriorInfoMatrixBSDM}) and Eq.~(\ref{eq:FactorPosteriorInfoMatrixBSBSP}). Thus, evaluating $J$ requires determinant calculation of an $n \times n$ (or $N \times N$) matrix, which is in general $O(n^3)$, per candidate action $a\in\mathcal{A}$. In many robotics applications state dimensionality can be huge and even increasing with time (e.g.~SLAM), and straight forward calculation of the above equations makes real-time planning hardly possible.


So far, the exposition referred to \unfocused BSP problems, where the action impact is calculated by considering \emph{all} the random variables in the system, i.e.~the entire state vector. However, as will be shown in the sequel, our approach is applicable also to \focused BSP problems.


\Focused BSP, in both augmented and non-augmented cases, is another important problem, where in contrast to the former case, only a subset of variables is of interest (see, e.g., \cite{Krause08jmlr, Levine13nips, Mu15rss}). For example one can look for action that reduces uncertainty of robot's final pose. The complexity of such a problem is much higher and proposed techniques succeeded to solve it in $O(kn^3)$ \cite{Krause08jmlr,Levine13nips} with $k$ being size of candidate actions set, and in $O(\tilde{n}^4)$ \cite{Mu15rss} with $\tilde{n}$ being size of involved clique within Markov random field representing the system.


Considering posterior entropy over the \focused variables $X^F_{k+L} \subseteq X_{k+L}$ we can write:
%
\begin{equation}
	J_{\mathcal{H}}^F(a) \!= \! \mathcal{H}(X^F_{k+L}) =
	\frac{n_F \cdot \gamma}{2} + \frac{1}{2} \ln \begin{vmatrix} \! \Sigma_{k+L}^{M,F} \! \end{vmatrix},
	\label{eq:ObjFuncPosteriorDeterminantFocused}
\end{equation}
%
where $n_F$ is the dimensionality of the state $X^F_{k+L}$, and $\Sigma_{k+L}^{M,F}$ is the posterior marginal covariance of $X^F_{k+L}$ (suffix $M$ for marginal), calculated by simply retrieving appropriate parts of posterior covariance matrix $\Sigma_{k+L}=\Lambda_{k+L}^{-1}$. 

Solving the above problem in a straightforward manner involves $O(N^3)$ operations for each candidate action, where $N = n+n'$ is dimension of posterior system. In the following sections we develop a computationally more efficient approach that addresses both \unfocused and \focused (augmented) BSP problems. As will be seen, this approach naturally supports non-myopic actions and arbitrary factor models $h_{i}^{j}$, and it is in particular attractive to belief space planning in high-dimensional state spaces.



