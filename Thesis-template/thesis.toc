
\save@tocdepth \setcounter {tocdepth}{2}
\select@language {english}
\contentsline {chapter}{List of Tables}{\relax }{chapter*.2}%
\contentsline {chapter}{Abstract}{1}{chapter*.3}%
\contentsline {chapter}{Abbreviations and Notations}{3}{chapter*.4}%
\contentsline {chapter}{\numberline {1}Introduction}{5}{chapter.5}%
\contentsline {section}{\numberline {\@@number {1.1}}Related Work}{6}{section.6}%
\contentsline {section}{\numberline {\@@number {1.2}}Contributions}{7}{section.7}%
\contentsline {section}{\numberline {\@@number {1.3}}Organization}{8}{section.8}%
\contentsline {chapter}{\numberline {2}Notations and Problem Formulation}{11}{chapter.16}%
\contentsline {chapter}{\numberline {3}Approach}{17}{chapter.34}%
\contentsline {section}{\numberline {\@@number {3.1}}BSP as Factor Graph}{17}{section.35}%
\contentsline {section}{\numberline {\@@number {3.2}}BSP via Matrix Determinant Lemma}{20}{section.41}%
\contentsline {subsection}{\numberline {\@@number {\@@number {3.2}.1}}Unfocused Case}{20}{subsection.42}%
\contentsline {subsection}{\numberline {\@@number {\@@number {3.2}.2}}Focused Case}{22}{subsection.46}%
\contentsline {section}{\numberline {\@@number {3.3}}Augmented BSP via AMDL}{23}{section.54}%
\contentsline {subsection}{\numberline {\@@number {\@@number {3.3}.1}}Augmented Matrix Determinant Lemma (AMDL)}{23}{subsection.55}%
\contentsline {subsection}{\numberline {\@@number {\@@number {3.3}.2}}Unfocused Augmented BSP through IG}{25}{subsection.62}%
\contentsline {subsection}{\numberline {\@@number {\@@number {3.3}.3}}Focused Augmented BSP}{27}{subsection.70}%
\contentsline {subsubsection}{\textbf {\texttt {Focused Augmented BSP ($X^F_{k+L} \subseteq X_{new}$)}} - focused variables belong to $G(a)$ }{27}{section*.71}%
\contentsline {subsubsection}{\textbf {\texttt {Focused Augmented BSP ($X^F_{k+L} \subseteq X_{old}$)}} - focused variables belong to $G_k$}{29}{section*.77}%
\contentsline {section}{\numberline {\@@number {3.4}}Re-use Calculations Technique}{30}{section.84}%
\contentsline {section}{\numberline {\@@number {3.5}}Connection to Mutual Information Approach and Theoretical Meaning of IG}{32}{section.87}%
\contentsline {section}{\numberline {\@@number {3.6}}Mutual Information - Fast Calculation via Information Matrix}{34}{section.94}%
\contentsline {chapter}{\numberline {4}Application to Different Problem Domains}{37}{chapter.99}%
\contentsline {section}{\numberline {\@@number {4.1}}Sensor Deployment}{37}{section.100}%
\contentsline {section}{\numberline {\@@number {4.2}}Augmented BSP in Unknown Environments}{40}{section.109}%
\contentsline {section}{\numberline {\@@number {4.3}}Graph Reduction}{46}{section.127}%
\contentsline {chapter}{\numberline {5}Alternative Approaches}{47}{chapter.131}%
\contentsline {chapter}{\numberline {6}Conclusions and Future Work}{49}{chapter.132}%
\contentsline {section}{\numberline {\@@number {6.1}}Future Work}{49}{section.133}%
\contentsline {chapter}{\numberline {7}Appendix - Related Publications}{51}{chapter.137}%
\contentsline {chapter}{\numberline {8}Appendix - Proof of Lemmas}{53}{chapter.145}%
\contentsline {section}{\numberline {\@@number {8.1}}Proof of Lemma \ref {lemma:AugMaxDetLemma}}{53}{section.146}%
\contentsline {section}{\numberline {\@@number {8.2}}Proof of Lemma \ref {lemma:AugMaxDetExtLemma}}{54}{section.155}%
\contentsline {section}{\numberline {\@@number {8.3}}Proof of Lemma \ref {lemma:FocNewEntropyLemma}}{55}{section.161}%
\contentsline {section}{\numberline {\@@number {8.4}}Proof of Lemma \ref {lemma:FocNewEntropyExtLemma}}{56}{section.168}%
\contentsline {section}{\numberline {\@@number {8.5}}Proof of Lemma \ref {lemma:FocOldEntropyLemma}}{57}{section.172}%
\contentsline {section}{\numberline {\@@number {8.6}}Proof of Lemma \ref {lemma:FocOldEntropyExtLemma}}{58}{section.182}%
\contentsline {section}{\numberline {\@@number {8.7}}SLAM Solution - focus on last pose $X^F_{k+L} \equiv x_{k+L}$}{59}{section.189}%
\contentsline {section}{\numberline {\@@number {8.8}}SLAM Solution - focus on mapped landmarks $X^F_{k+L} \equiv L_{k}$}{61}{section.196}%
\contentsline {section}{\numberline {\@@number {8.9}}Proof of Lemmas \ref {lemma:MIBasicLemma} and \ref {lemma:MIAdvancedLemma}}{64}{section.208}%
