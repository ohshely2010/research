Autonomous navigation in uncertain or unknown environments is essential in numerous applications in robotics, such as search and rescue, autonomous cars, indoor navigation, and surveillance. Once the robot operations take place in an unknown or uncertain environments, the navigation process also involves environment mapping. 
The corresponding problem, known as simultaneous localization and mapping (SLAM), has been extensively investigated \cite{Cadena16tro} in the last two decades by the robotics and computer vision communities. Computationally efficient online solvers that exploit the underlying inherent sparsity of the problem and re-use of calculations are readily available \cite{Kuemmerle11icra, Dellaert12tr, Agarwal16go}. 

%where due to the uncertainty of the environment and current location of the robot, one can only use probabilistic models in order to estimate the robot's poses to a given time. 
Traditional SLAM approaches include two parts, commonly known as the "front-end" and the "back-end". The latter maintains and updates a belief over robot past and current states (e.g.~poses) and mapped environment representation given the available data at each time instant. This data can include any prior information, if exists, performed actions and captured sensor observations with the corresponding data association (DA). The latter is determined by the front-end process, and can be considered as associating observed scenes (e.g.~in terms of landmarks) from current and previous time instances. A correct association between an observed landmark and a received measurement is crucial for accurate inference.

A key common assumption is that the data association has been correctly determined by the front-end. Such an assumption, however, is less valid in presence of perceptual aliasing and ambiguity. An incorrect data association can lead to catastrophic results in inference/SLAM, e.g. the robot might deduce it is located in an incorrect similar-looking corridor, while assuming it is perfectly solved within planning can lead to sub-optimal actions, that could lead to collision and unsafe behavior, in general.
\begin{figure}%[H]
	\includegraphics[width=0.5\linewidth]{Figures/diagram}
	\caption{Diagram illustrating considered problem. In this work we aim to calculate the probability of externally-defined hypothesis from some past time $k-p$ given the information that has been obtained until current time $k$. In other words, we would like to calculate $w^{i}_{k-p\mid k}=\prob{\gamma_{k-p}=i\mid H_{k}}$. The diagram illustrates, for simplicity, a branching factor of two, i.e.~each hypothesis branches at the next time into two child hypotheses (e.g.~due to obtained measurement with an ambiguous data association). For instance, we might be interested in calculating the weight $w_{k-p|k}^{i=1}$, which corresponds to probability of the hypothesis indicated by the {\color{blue} \textbf{blue}} node, given data until time $k$. }
	\label{fig:Diagram}
\end{figure}
Relaxing the data association assumption would lead to robust perception approaches that are much required while operating in real world environments \cite{Cadena16tro}, which typically exhibit some level of perceptual aliasing. Yet, this involves reasoning about DA as part of inference, and results in a set of candidate hypotheses, where each one is built by a possible landmark association to the given measurement in hand. Such a formulation corresponds to a multi-modal belief, that can be represented by a Gaussian mixture model (GMM) \cite{Pathak18ijrr, Tchuiev19iros},
\begin{eqnarray}
	b[X_{k}] = \prob{X_{k}\mid a_{0:k-1},z_{1:k}} = \sum_{i=1}^{M_{k}}w^{i}_{k}\cdot b^{i}[X_{k}],	
	\label{eq:GMMcurrent_time}
\end{eqnarray}
where $X_k$ is the joint state, and the $i$th belief component corresponds to a certain hypothesis regarding a sequence of data association in terms of a corresponding conditional posterior belief. The probability of that hypothesis is given by $w^{i}_{k}$.
In general, such an approach can be very computationally expensive with an exponential growths of GMM components over time. However, in practice, many of these components can be with negligible weight and thus can be pruned, and sufficiently similar components can be merged \cite{Pathak18ijrr, Tchuiev19iros}.


% since in  each step one needs to take into account all the possible landmark associations per a single hypothesis in the current GMM distribution, where in an highly aliased environment this yields in an exponential growth of the GMM number of components. 

While robust inference approaches have been actively investigated in the last few years, till now existing  approaches have dealt with relaxing the DA assumption while examining the state distribution at the \emph{current} time instant. Moreover, except of \cite{Hsiao19icra}, typically weight calculation is done from scratch for each time step, without calculation re-use.%In addition in all the cases currently known, the weight calculation as been done from scratch for each step in the robot's propagation. 

In contrast, in this work we propose the notion of \emph{hypotheses disambiguation in retrospective}, i.e.~after more information has been collected. Our approach enables to re-evaluate the probability of externally-defined, key strategic  hypotheses from a past time, given the information we obtained up to the current time, while accounting for the data association hypotheses developed since that past time. See illustration in Fig.~\ref{fig:Diagram}. For example, these hypotheses could refer to an observation of an important but ambiguous event (e.g.~did we observe scene/object A or B in the past?). 
We propose to utilize data that has been obtained since that time to update the posterior probabilities of these key past hypotheses. We envision such a capability and the general concept to be of interest in various contexts in robotics and beyond.%, such as hypotheses pruning and learning in retrospective.


Our main contributions in this paper are as follows: (a) we introduce the corresponding problem of hypotheses disambiguation in retrospective, which, to the best of our knowledge has not appeared thus far in literature; (b) we develop  a probabilistic approach to update the probability of selected past hypotheses considering a smoothing formulation, while  properly accounting for the ambiguous data association hypotheses that have been acquired since that time; (c) we derive a scheme for calculation re-use within this approach to reduce computational time; and (d) we evaluate our approach in simulation considering an extremely aliased environment comprising identical landmarks and show data association hypotheses indeed can be improved in retrospective.

\subsection{Related Work}

In the past years the research community has been actively investigating robust inference approaches  to be resilient to false data association overlooked by front-end algorithms, i.e.~by relaxing the assumption that the DA provided by front-end algorithms is outlier-free.  An  early work on DA is joint probability data association (JPDA) by Fortman et al. \cite{Fortmann80} which considers all possible DA options in the context of multi-target tracking.  Sunderhauf and Protzel \cite{Sunderhauf12iros} introduced the so called switchable constraints to detect faulty loop closures that lead to erroneous data association in back-end optimization. Olson and Agarwal \cite{Olson13ijrr} proposed a robust approach that uses max-mixture models. Carlone et al. \cite{Carlone14iros} addressed the problem from a different perspective, looking for a maximal coherent set among the given loop closure candidates. Indelman et al.~\cite{Indelman16csm} proposed a multi-robot framework for SLAM with ambiguous data association. Wong et al.~\cite{Wong13isrr} presented a Dirichlet Process Mixture Model (DPMM) for data association in partially observed environments. More recently, optimization approaches robust to outliers have been investigated in works such as \cite{Yang20ral, Antonante20arxiv}. Finally, \cite{Fourie16iros} and \cite{Hsiao19icra} develop incremental optimization approaches considering ambiguous data association.

A recent work by Pathak et al.~\cite{Pathak18ijrr} targets perceptual aliasing by explicitly reasoning about and probabilistically maintaining ambiguous DA hypotheses, in both inference and belief space planning.  In contrast to many of the works mentioned above,  it explicitly calculates the probability of different hypotheses, i.e.~weights of GMM components, rather than assuming these to be identical. Tchuiev et al. \cite{Tchuiev19iros} extend the passive inference formulation from \cite{Pathak18ijrr} by utilizing semantic information and viewpoint-dependent classifier models, as well as weight pruning to reduce the number of DA hypotheses. 

While the above-mentioned approaches address inference considering the (GMM) belief from the current time, we investigate a complimentary aspect, namely, utilizing current information to re-evaluate the probability of past, externally-specified, DA hypotheses. Specifically, building upon \cite{Pathak18ijrr, Tchuiev19iros} we develop a smoothing approach for updating the weights of past GMM beliefs, while properly accounting for the ambiguous data association hypotheses that have been acquired since then. Furthermore, we show how to do so while re-using calculations.



%Yet again all of the above approaches address the current belief distribution given the information gathered up to the current time, our approach handles the more general case where we examine how current information can effect our evaluation of a given past belief.

%===

%Finally, recently Fourie et al.~\cite{Fourie16iros} addressed computational aspects, aiming to update the GMM belief incrementally. One limitation of their approach, however, is that the association probabilities of a new measurement to different scenes/objects are assumed to have a uniform distribution, i.e. identical weights. 

%It is important to mention that all of the mentioned works, addressed only the passive case. \cite{Pathak18ijrr} deals with both the passive and active case, and in contrary to other works assumes the weight distribution between the different hypotheses is none uniform.





% Sunderhauf and Protzel \cite{Sunderhauf12iros} takes this approach while examining false loop closures detection due to an ambiguous setting. Other works, including  \cite{Carlone14iros, Indelman16csm, Sunderhauf12icra} use a graph optimization aspect, \VI{when in all this cases the DA assumption was relaxed and handled in the passive case where the set actions is given. [revisit]}

%However in all the following cases our belief on the robot's current location was estimated given information of the current time, we address a more general case, where information from different time frames might correlate.
%Using a mixture model of probabilistic functions was also done by Wong in \cite{Wong13isrr}, which uses Dirichlet Process mixture modele instead of standard Gaussian distribution. 



%Our research also deals with the ability to disambiguate between different hypotheses in our GMM, and in certain cases isolate the correct one.  This was also addressed by \cite{Atanasov14tro,Lauri15rssws,Patten16ral,Sankaran15arxiv} in the form of object detection and classification, where they assumed the map is known and use a set of future viewpoints to distinguish between current hypotheses, however this approaches in some cases can take into account a null hypotheses that might have been obsolete in the compare to the other hypotheses,

% Fourie for instance in \cite{Fourie16iros} while addressing the computational aspect by performing an incremental update of the hypotheses using a graphical optimization, assumes a uniform distribution of the GMM weights.
%Agarwal in \cite{Agarwal15arxiv} deals with relaxing the DA assumption as well, altougth he handles only the case of full disambiguation of the current GMM via planning, and assumes the ambiguity is only in the current belief, this can be problematic in an highly ambiguous setting.
 
%It is important to mention that as far as we know most of the work done today is done in the passive case while Pathak in \cite{Pathak18ijrr} also handles the active case, and builds a frame work for belief space planning while releaxing the DA assumption, he also addresses the general case where the weight distribution is non uniform, and the calculated set of actions does not always leads to full disambiguation.
%We should also mention Tchuiev in \cite{Tchuiev19iros} that in appose to other approaches that based only on viewpoint geometry tries to create a viewpoint dependent classifier model, such that will be able to indentify the correct landmark associated to the current measurement regardless of the robot's viewpoint geometry.

%Yet again all of the above approaches address the current belief distribution given the information gathered up to the current time, our approach handles the more general case where we examine how current information can effect our evaluation of a given past belief. %\OS{This can be used for instance in order to reduce the exponential growth of the number of hypotheses in our GMM, in an highly alised enviorement[Not sure this is needed?]}. 
%In addition we will show a re-use of preivous calculations to update the current weight distribution, when most of the known approaches perform this calculation from scratch for each step in the robot's trajecotry.

%==



%A first work that addressed data association aspects also within planning is \cite{Pathak18ijrr}, paving the way to autonomous disambiguation between data association hypotheses while accounting for different sources of uncertainty. The main focus of that work was on the planning aspect, where active disambiguation was realized by defining a cost function that involves the GMM belief weights - specifically, a KL divergence pseudo-metric was calculated between the GMM components weights and a uniform distribution, to motivate ”spiky” hypothesis weights, which corresponds to full disambiguation. 

%However, relaxing the DA assumption introduces a number of challenges. First, in a highly aliased environment, the number of GMM components grows exponentially, as for each new observation, one has to consider the possible association hypotheses, which multiple the number of GMM belief components from the previous step. 

%Second, the weight calculation in all current works is performed from scratch for each step we take up to the current time.
