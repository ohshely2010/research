\subsection{Notations}
Let $x_{k}$ represent the robot current pose at time $k$, and denote by  $X_{k}=\{x_0,x_1,...,x_k\}$ all robot poses until that time. 
%\VI{[only poses, but not landmarks?]}\OS{[Do you wish this to be a defition of the state, not only poses?]} 
We define $u_{k}$ and $z_k$ to be, respectively, the robot's action and captured observation at time $k$. Further, we represent the environment by landmarks $L=\{l_i\}_{i=1}^{N_L}$, and assume they are static and known.

Further, denote data association (DA) at time $k$ by a discrete latent variable $\beta_{k}$, i.e.~measurement $z_k$ is associated to landmark (or object/scene) $l_{\beta_{k}=p}$, where $p \in [1,N_{L}]$.  It is important to note that in the general case of obtaining a number of measurements in a single step,  $\beta_{k}$ would have been addressed to as a vector built from the landmarks associated to each measurement, as done in \cite{Tchuiev19iros}.

%\VI{[is this assumption a must? for example, in Tchuiev19iros the formulation is more general.]}\OS{[From what I saw in the article, is $n_{i}$ measurements for a given time $i$.]}

We use motion and observation models with additive Gaussian noise,
\begin{eqnarray}
	x_{k} = f(x_{k-1},u_{k-1}) + w \;\;,\;\;  z_{k} = h(x_k,l_{\beta_k}) + v,
	\label{eq:models}
\end{eqnarray}
where $w \sim \mathcal{N}(\mu_{w},\Sigma_{w})$ and $v \sim \mathcal{N}(\mu_{v},\Sigma_{v})$. The process and measurement covariance matrices, $\Sigma_w$ and $\Sigma_v$, as well as the functions $f(.)$ and $h(.)$ are assumed to be known.

Let history $H_{k}$ represent all the robot's actions and received measurements till time $k$ along with the set of known landmarks $L$, $H_{k}=\{z_{1:k},u_{0:k-1},L\}$. Similarly, denote by  $H^{-}_{k}$ history without the received measurement at time $k$,  $H^{-}_{k}=\{z_{1:k-1},u_{0:k-1},L\}$.
 The probability density function (pdf), the \emph{belief}, at time $k$ over $X_k$ is then given by,
\begin{eqnarray}
	\label{GMM_passive_case}
	b[X_{k}] =\prob{X_{k}\mid H_{k}}.				
\end{eqnarray}
We should note that in the general case where the landmarks are unknown  we will add to our state at \eqref{GMM_passive_case} a vector of the gathered landmarks up to time $k$.
Since we consider ambiguous scenarios, one cannot assume data association to be given and perfect. Similar to \cite{Pathak18ijrr,Tchuiev19iros}, 
%we maintain probabilistically data association hypotheses over time. More concretely, consider at each time $i\in[1,k]$ until the current time $k$, the latent variable $\beta_i$ denoting data association for measurement $z_i$. 
the number of hypotheses at time $k$ (without yet considering pruning) is given by all possible realizations of the sequence $\beta_{1:k}=\{\beta_1, \ldots, \beta_k\}$.  For convenience we denote $\gamma_k \doteq \beta_{1:k}$, and consider at time $k$ to have $M_k$ hypotheses, i.e.~$\gamma_{k} \in [1, M_k]$. Thus, the $i$th hypothesis, i.e.~$\gamma_{k}=i$, corresponds to a specific realization of the sequence $\beta_{1:k}$.

Hence, by performing marginalization of \eqref{GMM_passive_case} over $\gamma_{k}$, and chain rule,
%
%\VI{In practice relaxing the given data association assumption, is in fact marginalization of the belief \eqref{GMM_passive_case} over all possible data association realizations $\gamma_k$, instead of considering the latter to be given. [confusing, in \eqref{GMM_passive_case} it is currently unclear you assume DA to be given]} Doing so, followed by chain rule, gives us a GMM with $M_k$ components,	
%
\begin{eqnarray}
	\label{eq:GMM_weight_value_pre}
	b[X_k] =% &&= \sum_{i=1}^{M_{k}}\prob{X_{k},\gamma_{k} = i\mid H_{k}} = \\
	%&&
	\sum_{i=1}^{M_{k}} \underbrace{\prob{\gamma_{k}=i|H_{k}}}_\text{$w^{i}_{k}$}\cdot
	\underbrace{\prob{X_{k}\mid \gamma_{k} = i,H_{k}}}_\text{$b^{i}[X_k]$},  %\nonumber
\end{eqnarray}
where 
$b^{i}[X_k]$ and $w^{i}_{k}$ represent, respectively, the conditional belief and the weight of the $i$th hypothesis at time $k$. As a result of chain rule used in \eqref{eq:GMM_weight_value_pre},  $\sum_{i=1}^{M_{k}}w^{i}_{k}=1$, which corresponds to assuming one of the hypotheses to be the correct one.

Finally, we denote the propagated belief as the belief conditioned on $H_k^-$ instead of $H_k$, i.e.~without considering the measurement at the current time, $z_{k}$. Similarly, a propagated belief for the $i$th hypothesis is defined as  $b^{i-}[X_{k}]=\prob{X_{k}\mid \gamma_{k}=i,H^{-}_{k}}$.

\subsection{Problem Formulation}
%\OS{[Add the fact that in this work we re eveluate $\gamma$, another option would have been to eveluate $\beta$ per a given time. Another issue is the correlation between two hypotheses from diffrent times.]}
In this work we wish to re-evaluate, in retrospective, the probability of externally-specified hypothesis (or hypotheses) from some past time, i.e.~given new information acquired since then.
In other words we wish to re-evaluate an hypothesis weight for a given $\gamma_{k-p}=i$, another  option would have been to re-evaluate the possibility of viewing the associated landmark, aka $\beta_{k-p}$.

%weight of a hypothesis from some previous time, using "new" information acquired since then. 
Specifically, our goal is to calculate 
\begin{equation}
	\label{eq:problem_formulation}
	w^{i}_{k-p\mid k} \triangleq \prob{\gamma_{k-p}=i\mid H_{k}},
\end{equation}
where $1 \leq p < k$. To shorten notations in the sequel, we denote $m \doteq k-p$ and re-write \eqref{eq:problem_formulation} as $w^{i}_{m\mid k} \doteq \prob{\gamma_{m}=i\mid H_{k}}$.

%and in particular focus on calculating the corresponding GMM weights $w^{i}_{k+p\mid k+L}$.\\
%This can have effect in the passive case, where we wish to reevaluate a certain hypothesis weight in retrospective. As mentioned we can address this approach as "future smoothing".			 	
%
In other words, in this work we investigate a smoothing perspective considering discrete random variables.

This approach can be beneficial in both re-evaluation of a given hypothesis reliability, as well for correlation between two hypotheses from diffrent time frames.

