\begin{thebibliography}{1}

\bibitem{Smith87}
R.~Smith and P.~Cheeseman.
\newblock On the representation and estimation of spatial uncertainty.
\newblock {\em Intl. J. of Robotics Research}, 5(4):56--68, 1987.

\bibitem{DurrantWhyte88tra}
H.F. Durrant-Whyte.
\newblock Uncertain geometry in robotics.
\newblock {\em {IEEE} Trans. Robot. Automat.}, 4(1):23--31, 1988.

\bibitem{Indelman16csm}
V.~Indelman, E.~Nelson, J.~Dong, N.~Michael, and F.~Dellaert.
\newblock Incremental distributed inference from arbitrary poses and unknown
  data association: Using collaborating robots to establish a common reference.
\newblock {\em IEEE Control Systems Magazine (CSM), Special Issue on
  Distributed Control and Estimation for Robotic Vehicle Networks},
  36(2):41--74, 2016.

\bibitem{Lee13iros}
Gim~Hee Lee, Friedrich Fraundorfer, and Marc Pollefeys.
\newblock Robust pose-graph loop-closures with expectation-maximization.
\newblock In {\em IEEE/RSJ Intl. Conf. on Intelligent Robots and Systems
  (IROS)}, pages 556--563, 2013.

\bibitem{Sunderhauf13icra}
N.~S\"{u}nderhauf and P.~Protzel.
\newblock Switchable constraints vs. max-mixture models vs. {RRR} - a
  comparison of three approaches to robust pose graph {SLAM}.
\newblock In {\em IEEE Intl. Conf. on Robotics and Automation (ICRA)}, 2013.

\bibitem{Carlone14iros}
L.~Carlone, A.~Censi, and F.~Dellaert.
\newblock Selecting good measurements via l1 relaxation: A convex approach for
  robust estimation over graphs.
\newblock In {\em IEEE/RSJ Intl. Conf. on Intelligent Robots and Systems
  (IROS)}, pages 2667--2674. IEEE, 2014.

\bibitem{Fourie16iros}
D.~Fourie, J.~Leonard, and M.~Kaess.
\newblock A nonparametric belief solution to the bayes tree.
\newblock In {\em IEEE/RSJ Intl. Conf. on Intelligent Robots and Systems
  (IROS)}, 2016.

\bibitem{Pathak18ijrr}
S.~Pathak, A.~Thomas, and V.~Indelman.
\newblock A unified framework for data association aware robust belief space
  planning and perception.
\newblock {\em Intl. J. of Robotics Research}, 32(2-3):287--315, 2018.

\bibitem{Tchuiev19iros}
Vladimir Tchuiev, Yuri Feldman, and Vadim Indelman.
\newblock Data association aware semantic mapping and localization via a
  viewpoint-dependent classifier model.
\newblock In {\em IEEE/RSJ Intl. Conf. on Intelligent Robots and Systems
  (IROS)}, 2019.

\end{thebibliography}
